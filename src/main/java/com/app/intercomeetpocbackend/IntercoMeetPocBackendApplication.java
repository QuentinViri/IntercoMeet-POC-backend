package com.app.intercomeetpocbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntercoMeetPocBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntercoMeetPocBackendApplication.class, args);
	}

}
